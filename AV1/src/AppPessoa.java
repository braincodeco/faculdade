import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;

public class AppPessoa {

	static ArrayList<Pessoa> lista;
    static Scanner tecla;
    
    static {
        AppPessoa.lista = new ArrayList<Pessoa>();
        AppPessoa.tecla = new Scanner(System.in);
    }
    
    public static void main(final String[] args) {
        popularLista();
        ordenarLista();
        int op;
        do {
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Para consultar Pessoa");
            System.out.println("2-Para listar Pessoas");
            System.out.println("3-Sair");
            System.out.println("Digite sua op��o: ");
            op = AppPessoa.tecla.nextInt();
            switch (op) {
                default: {
                    continue;
                }
                case 1: {
                    consultarPessoa();
                    continue;
                }
                case 2: {
                    listarPessoas();
                    continue;
                }
            }
        } while (op != 3);
    }
    
    public static void popularLista() {
    	AppPessoa.lista.add(new Pessoa(1, 10, "Jo�o"));
    	AppPessoa.lista.add(new Pessoa(2, 5, "Alice"));
    	AppPessoa.lista.add(new Pessoa(3, 27, "Fernando"));
    	AppPessoa.lista.add(new Pessoa(4, 12, "Carlos"));
    	AppPessoa.lista.add(new Pessoa(5, 31, "Priscila"));
    }
    
    public static void ordenarLista() {
        Collections.sort(AppPessoa.lista);
    }
    
    public static void listarPessoas() {
        for (final Pessoa pessoa : AppPessoa.lista) {
            System.out.println("codigo : " + pessoa.getCod());
            System.out.println("idade : " + pessoa.getIdade());
            System.out.println("nome : " + pessoa.getNome());
            System.out.println("----------------------------");
        }
    }
    
    public static void consultarPessoa() {
        System.out.println("Digite o id Pessoa:");
        final int num = AppPessoa.tecla.nextInt();
        Pessoa pessoacache = null;
        pessoacache = CachePessoa.buscarCache(num);
        if (isNotNullAndNotEmpty(pessoacache)) {
            System.out.println("codigo : " + pessoacache.getCod());
            System.out.println("idade : " + pessoacache.getIdade());
            System.out.println("nome : " + pessoacache.getNome());
            System.out.println("Buscado do cache");
            System.out.println("----------------------------");
        }
        else {
            for (final Pessoa pessoa : AppPessoa.lista) {
                if (pessoa.getCod() == num) {
                    CachePessoa.addPessoaCache(pessoa);
                    System.out.println("codigo : " + pessoa.getCod());
                    System.out.println("idade : " + pessoa.getIdade());
                    System.out.println("nome : " + pessoa.getNome());
                    System.out.println("Buscado da lista principal");
                    System.out.println("----------------------------");
                }
            }
        }
    }
    
    public static boolean isNotNullAndNotEmpty(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof String) {
            final String s = (String)obj;
            if (s.trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
