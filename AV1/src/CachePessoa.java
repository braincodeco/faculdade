import java.util.Iterator;
import java.util.ArrayList;

public class CachePessoa {

static ArrayList<Pessoa> listaCache;
    
    static {
        CachePessoa.listaCache = new ArrayList<Pessoa>();
    }
    
    public static ArrayList<Pessoa> addPessoaCache(final Pessoa pessoa) {
        CachePessoa.listaCache.add(new Pessoa(pessoa.getCod(), pessoa.getIdade(), pessoa.getNome()));
        return CachePessoa.listaCache;
    }
    
    public static Pessoa buscarCache(final int cod) {
        Pessoa pessoaretorno = null;
        for (final Pessoa pessoa : CachePessoa.listaCache) {
            if (pessoa.getCod() == cod) {
                pessoaretorno = pessoa;
            }
        }
        return pessoaretorno;
    }
}
