import java.util.Scanner;

public class Exercicio6 {

	//Declara��o de variaveis globais
	static Scanner tecla = new Scanner (System.in);
	
	public static void main(String[] args) {

	//Declara��o de variaveis locais e constantes
	double odomi, odomf, litro, valort, media, lucro, gasolina;
	gasolina = 1.90;	
		
	
	//Entrada de dados
	System.out.println("Marca��o inicial do odometro (KM): ");	
	odomi = tecla.nextDouble();	
	System.out.println("Marca��o final do odometro (KM): ");	
	odomf = tecla.nextDouble();		
	System.out.println("Quantidade de combust�vel gasto (Litro): ");	
	litro = tecla.nextDouble();	
	System.out.println("Valor total recebido (R$): ");	
	valort = tecla.nextDouble();	
	
	//Processamento de dados	
	media = (odomf - odomi) / litro;	
	lucro = valort - (litro * gasolina);
		
		
	//Saida de dados
	System.out.println("Media de consumo em KM/L: " + media);		
	System.out.println("Lucro liquido do dia: " + lucro);	
		
	}

}