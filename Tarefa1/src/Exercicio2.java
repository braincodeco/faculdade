import java.util.Scanner;

public class Exercicio2 {
	
	// DECLARA��O DE VARIAVEIS GLOBAIS (static)
		static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// DECLARA��O DE VARIAVEIS LOCAIS E CONSTANTES
		double temp_f, temp_c;
		
		// ENTRADA DE DADOS
		System.out.println("Informe a temperatura em graus Fahrenheit: ");
		temp_f = tecla.nextDouble();
		
		// PROCESSAMENTO DE DADOS
		temp_c = ((temp_f - 32) * 5) / 9;
		
		// SAIDA DA INFORMA��O
		System.out.println("A temperatura em graus Celsius �: " + temp_c);

	}

}
