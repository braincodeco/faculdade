
import java.util.Scanner;

public class Exercicio1 {

	// DECLARA��O DE VARIAVEIS GLOBAIS (static)
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		//DECLARA��O DE VARIAVEIS LOCAIS E CONSTANTES
		
		double raio, area;
		final double PI = 3.14;
		
		// ENTRADA DE DADOS
		
		System.out.println("Digite o valor do raio: ");
		raio = tecla.nextDouble();
		
		// PROCESSAMENTO DE DADOS
		
		area = PI * Math.pow(raio,2);
		
		//SAIDA DA INFORMA��O
		System.out.println("�rea do circulo: " + area);

	}

}
