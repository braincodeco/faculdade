package Exercicio4;

import java.util.ArrayList;
import java.util.Scanner;

public class App_Estoque {

static ArrayList<Produto> lista = new ArrayList<>();
    
    static Scanner tecla = new Scanner(System.in);
    
	public static void main(String[] args) {
		 int op;
	        do {                
	            System.out.println("*** MENU PRINCIPAL ***");
	            System.out.println("(1)-Incluir Produto");
	            System.out.println("(2)-Lista de Estoque Baixo");
	            System.out.println("(3)-Excluir Produto");
	            System.out.println("(4)-Total de Reais em Estoque");
	            System.out.println("(5)-Lista de todos os Produtos");
	            System.out.println("(6)-Sair");
	            System.out.println("Digite sua op��o: ");
	            op = tecla.nextInt(); 
	            switch(op){
	                case 1: incluirProduto(); break;
	                case 2: estoqueBaixo(); break;
	                case 3: excluirProduto(); break;
	                case 4: totalEmReaisEstoque(); break;
	                case 5: listarProdutos(); break;
	                case 6: break;
	            }
	        } while (op!=6);     

	}

	

	private static void listarProdutos() {
		System.out.println("=====================================");	
		System.out.println("======== Lista de Produtos ==========");
		System.out.println("=====================================");
		for(Produto produto : lista) {
			System.out.println("Codigo do Produto:  " + produto.getCodProduto());	
			System.out.println("Nome do Produto:  " + produto.getNome());
			System.out.println("Pre�o do Produto:  " + produto.getPreco());
			System.out.println("Quantidade do Produto:  " + produto.getQuantidadeEmEstoque());
			System.out.println("==================================");
		}
		
	}



	private static void totalEmReaisEstoque() {
		
		 double total = 0;
	        for (Produto produto : lista) {
					
				total += (produto.getPreco() * produto.getQuantidadeEmEstoque());
				}
	        System.out.println("Total:....." + total);
	        System.out.println("====================");	
			}
	        
	    



	private static void excluirProduto() {
		System.out.println("Digite o Codigo do Produto:");
        int num = tecla.nextInt();
        
        //Procurar a conta na lista para exclus�o
        for (Produto produto : lista) {
			if (produto.getCodProduto() == num) {
				lista.remove(produto);
				System.out.println("Produto Excluido com sucesso!");
				System.out.println("=============================");
				break;
			}
		}
		
	}



	private static void estoqueBaixo() {
		System.out.println("Qual a quantidade minima no estoque do produto ?:");
		int num = tecla.nextInt();
		for (Produto produto : lista) {
			if (produto.getQuantidadeEmEstoque() < num) {
				System.out.println("===========================================");
				System.out.println("O Produto precisa ser reposto:" + produto.getNome());
				System.out.println("===========================================");
			}
		}
		
	}



	private static void incluirProduto() {
		 System.out.println("Digite o codigo do Produto:");
	        int num = tecla.nextInt();
	        System.out.println("Digite o nome do Produto:");
	        String nome = tecla.next();
	        System.out.println("Digite o preco do Produto:");
	        double preco = tecla.nextDouble();	       
	        System.out.println("Digite a quantidade em estoque do Produto:");
	        int quantidade = tecla.nextInt();
	        lista.add(new Produto(num,nome,preco,quantidade));
	        System.out.println("Produto cadastrado com sucesso!");	
	        System.out.println("===========================================");
	}
	

}

